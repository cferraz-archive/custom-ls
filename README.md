# custom-ls

is a little experiment with livescript, it translates custom operators to infix functions, it's very experimental and should not be used

Check out **[livescript.net](http://livescript.net)** for more information, examples, usage, and a language reference.

### Using

Declare the operators as:

    #:[["operator", "replacement"]]

Note that both strings must have equal lengths to not crap with the sourcemaps.
Example ideal use:

    #:[[" $ ", "`$`"]]

    x $ y # gets translated to x `$` y that happens to works like $(x, y)

#### Other Changes

* Added `a >: b` that works like `a.chain b`;
* Added `a <: b` that works like `a <- b.chain`;
* Added a runtime to avoid resource waste;
* Improved curry speed.

Basically:
  * Added runtime library (`cls-tools`);
  * Replaced curry function to one that limits the number of arguments to 9;
  * Added monad operators.

### Build Status
[![Build Status](https://travis-ci.org/masterrace/custom-ls.svg?branch=master)](https://travis-ci.org/masterrace/custom-ls)

### Install
Have Node.js installed. `sudo npm install -g custom-ls`

After, run `lsc -h` for more information.


### LiveScript Repo
[git://github.com/gkz/LiveScript.git](git://github.com/gkz/LiveScript.git)

### LiveScript Community

If you'd like to chat, drop by [#livescript](irc://irc.freenode.net/livescript) on Freenode IRC.
If you don't have IRC client you can use Freenode [webchat](https://webchat.freenode.net/?channels=#livescript).
